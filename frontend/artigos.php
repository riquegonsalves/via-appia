
	<?php include 'includes/head.php' ?>
	<?php include 'includes/header.php' ?>
  <div class="title-banner">
  	<div class="container">
  		<h1>ARTIGOS E NOTÍCIAS</h1>
			<h2>informação relevante para quem quer se informar mais sobre a Via Appia e sobre a área de tecnologia em si</h2>
  	</div>
		<div class="seta">
			<div class="circle">
				<span>&nbsp;</span>
			</div>
		</div>
  </div>

	<div class="page page-articles">
		<div class="container">
			<ul>
				<li>
					<a href="#">
						<div class="text">
							<div class="category">
								<span>Artigo</span>
							</div>
							<h2>Las cuatro prioridades para las bibliotecas del futuro</h2>
							<h4>por: Jualian Marquina</h4>
							<h4>29/05/2013</h4>

						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="text">
							<div class="category">
								<span>Artigo</span>
							</div>
							<h2>Las cuatro prioridades para las bibliotecas del futuro</h2>
							<h4>por: Jualian Marquina</h4>
							<h4>29/05/2013</h4>

						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="text">
							<div class="category">
								<span>Artigo</span>
							</div>
							<h2>Las cuatro prioridades para las bibliotecas del futuro</h2>
							<h4>por: Jualian Marquina</h4>
							<h4>29/05/2013</h4>

						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="text">
							<div class="category">
								<span>Artigo</span>
							</div>
							<h2>Las cuatro prioridades para las bibliotecas del futuro</h2>
							<h4>por: Jualian Marquina</h4>
							<h4>29/05/2013</h4>

						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="text">
							<div class="category">
								<span>Artigo</span>
							</div>
							<h2>Las cuatro prioridades para las bibliotecas del futuro</h2>
							<h4>por: Jualian Marquina</h4>
							<h4>29/05/2013</h4>

						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="text">
							<div class="category">
								<span>Artigo</span>
							</div>
							<h2>Las cuatro prioridades para las bibliotecas del futuro</h2>
							<h4>por: Jualian Marquina</h4>
							<h4>29/05/2013</h4>

						</div>
					</a>
				</li>

			</ul>
		</div>
	</div>
	<?php include 'includes/footer.php' ?>
