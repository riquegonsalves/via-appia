<?php include 'includes/head.php' ?>
<?php include 'includes/header.php' ?>
<div class="siab-banner">
  <div class="container">
      <img src="img/siab/banner.png" alt="" />
      <p>
        Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Aliquam eu nunc. Praesent congue erat at massa. Nullam sagittis.
      </p>
    <a href="#" class="btn">Tenho Interesse</a>
  </div>

  <div class="seta">
    <div class="circle">
      <span>&nbsp;</span>
    </div>
  </div>
</div>

<div class="page page-siab">
  <div class="container about">
    <div class="img">
      <img src="img/siab/config.png" alt="" />
    </div>
    <div class="text">
      <h1>SOBRE O SEARCH IN A BOX</h2>
      <p>
        Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Nulla porta dolor. Nam eget dui. Fusce fermentum odio nec arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. </p>
      <p>
        Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Nulla porta dolor. Nam eget dui. Fusce fermentum odio nec arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. </p>

      <a href="#" class="btn">Tenho Interesse</a>

    </div>
  </div>
  <div class="high-tec">
    <div class="container">
      <h2>ALTA TECNOLOGIA</h2>
      <h3>SIMPLES DE USAR</h3>
      <p>
        Basta conectar na unidade de armazenamento que se deseja analisar e o Search-in-a-box fará todo o trabalho por você
      </p>
    </div>
  </div>
  <div class="perfect">
    <div class="container">
      <div class="text">
        <h2>PERFEITO PARA A SUA NECESSIDADE</h2>
        <h3>O Search-in-a-box é um produto de potencial múltiplo e ajuda a sanar diversos tipos de necessidade </h3>

        <ul>
          <li>
            <div class="icon bigdata">

            </div>
            <h4>Big Data</h4>
            <p>Curabitur at lacus ac velit ornare lobortis. Duis lobortis massa imperdiet quam.</p>
          </li>
          <li>
            <div class="icon redesocial">

            </div>
            <h4>Big Data</h4>
            <p>Curabitur at lacus ac velit ornare lobortis. Duis lobortis massa imperdiet quam.</p>
          </li>
          <li>
            <div class="icon investigacao">

            </div>
            <h4>Big Data</h4>
            <p>Curabitur at lacus ac velit ornare lobortis. Duis lobortis massa imperdiet quam.</p>
          </li>
        </ul>
        <a href="#" class="btn">Tenho Interesse</a>


      </div>
      <div class="img">
        <img src="img/siab/perfect.png" alt="" />
      </div>
    </div>
  </div>
  <div class="configuration">
    <div class="container">
      <h2>CONFIGURAÇÃO</h2>
      <div class="infos">
        <div class="left">
          <div class="info">
            <div class="icon softbasic"></div>
            <h4>Software Básico</h4>
            <p>• Sistema Operacional do appliante</p>
            <p>• IBM Watson Explorer versão mais atual</p>
            <p>• Via Appia TheXML versão 2.1</p>
          </div>
          <div class="info">
            <div class="icon softopc"></div>
            <h4>Software Opcional</h4>
            <p>• IBM Infosphere Streams</p>
            <p>• IBM Infosphere Big Insights</p>
          </div>
        </div>
        <div class="mid">
          <img src="img/siab/config.png" alt="" />
        </div>
        <div class="right">
          <div class="info">
            <div class="icon hardbasic"></div>
            <h4>Hardware Básico</h4>
            <p>• Gabinete para Hack 2U</p>
            <p>• 2TB de HD SATA 7200RPM em RAID 1 ou 4 TB simples</p>
          </div>
          <div class="info">
            <div class="icon hardext"></div>
            <h4>Hardware Extensível</h4>
            <p>• 16TB de HD em RAID 1 ou 32 TB simples</p>
            <p>• Possibilidade de adição de mais um processador</p>
            <p>• Possibilidade de configuração distribuída com múltiplos dispositivos</p>
          </div>
        </div>
      </div>
      <a href="#" class="btn">Tenho Interesse</a>
    </div>
  </div>
</div>

<?php include 'includes/footer.php' ?>
