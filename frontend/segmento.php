<?php include 'includes/head.php' ?>
<?php include 'includes/header.php' ?>
<div class="title-banner">
  <div class="container">
      <a href="#" class="back-button"><i class="back"></i> VOLTAR</a>
    <h1 class="title-segmento">THE XML</h1>
    <h2>produtos e serviços que compõem o roll de soluções da Via Appia</h2>
    <a href="#" class="button">Tenho Interesse</a>
  </div>

  <div class="seta seta-segmento">
    <div class="circle">
      <span>&nbsp;</span>
    </div>
  </div>
</div>

<div class="page page-segmento">
  <div class="container screenbg">
    <h1>PARA QUE SERVE?</h1>
    <div class="scroll-arrows">

    </div>

    <div class="screen">
      <div class="black" id="blackboard">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
    </div>
    <div class="screenBase">

    </div>
  </div>
  <div class="container">
    <div class="essencial">
      <h1>ESSENCIAL PARA A SUA EMPRESA</h1>
      <ul>
        <li>
          <div class="num">
            1
          </div>
          <p>Tenha tal benefício e resolva os problemas da sua empresa de forma espetacular</p>
        </li>
        <li>
          <div class="num">
            2
          </div>
          <p>Tenha tal benefício e resolva os problemas da sua empresa de forma espetacular</p>
        </li>
        <li>
          <div class="num">
            3
          </div>
          <p>Tenha tal benefício e resolva os problemas da sua empresa de forma espetacular</p>
        </li>
      </ul>
      <a href="#">Veja nossas soluções</a>
    </div>
    <div class="solutions">
      <h1>NOSSAS SOLUÇÕES EM < SEGMENTO > </h1>
      <div class="carousel">
        <div class="visible">
          <ul>
            <li>
              <a href="#">
                <div class="main">
                  <div class="title">

                    <div class="icon">
                      <img src="img/portfolio/xml.png" alt="" />
                    </div>
                    <h3>Search-in-a-box</h3>
                  </div>
                  <div class="text">
                    <p>
                      O dispositivo de appliance que organiza todos os seus arquivos e permitir buscas intuitivas para a análise inteligente dos dados da sua empresa. Tudo isso de forma simples e rápida!
                    </p>
                  </div>
                </div>
                <a href="#" class="more">
                  saber mais...
                </a>
              </a>
            </li>
            <li>
              <a href="#">
                <div class="main">
                  <div class="title">

                    <div class="icon">
                      <img src="img/portfolio/xml.png" alt="" />
                    </div>
                    <h3>Search-in-a-box</h3>
                  </div>
                  <div class="text">
                    <p>
                      O dispositivo de appliance que organiza todos os seus arquivos e permitir buscas intuitivas para a análise inteligente dos dados da sua empresa. Tudo isso de forma simples e rápida!
                    </p>
                  </div>
                </div>
                <a href="#" class="more">
                  saber mais...
                </a>
              </a>
            </li>
            <li>
              <a href="#">
                <div class="main">
                  <div class="title">

                    <div class="icon">
                      <img src="img/portfolio/xml.png" alt="" />
                    </div>
                    <h3>Search-in-a-box</h3>
                  </div>
                  <div class="text">
                    <p>
                      O dispositivo de appliance que organiza todos os seus arquivos e permitir buscas intuitivas para a análise inteligente dos dados da sua empresa. Tudo isso de forma simples e rápida!
                    </p>
                  </div>
                </div>
                <a href="#" class="more">
                  saber mais...
                </a>
              </a>
            </li>

          </ul>

        </div>
        <div class="arrow-nav">
          <div class="prev active">

          </div>
          <div class="next active">

          </div>
        </div>
      </div>
      <a href="#" class="btn">Tenho Interesse</a>
    </div>
  </div>

  <div class="qualidade">
    <div class="container">
      <h1>ALTO PADRÃO DE QUALIDADE</h1>
      <p>Curabitur nisi. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Vivamus consectetuer hendrerit lacus. Praesent ac sem eget est egestas volutpat. Nunc nonummy metus.</p>
      <ul class="selos">
        <li>
          <img src="img/seloiso.png" alt="ISO 9001" />
        </li>
        <li>
          <img src="img/seloiso.png" alt="ISO 9001" />
        </li>
        <li>
          <img src="img/seloiso.png" alt="ISO 9001" />
        </li>
      </ul>
      <div class="scroll-arrows">

      </div>
    </div>
  </div>
  <div class="container">
    <ul class="clientes">
      <li>CLIENTES:</li>
      <li><a href="#"><img src="img/clientes/ceub.png" alt="" /></a></li>
      <li><a href="#"><img src="img/clientes/bc.png" alt="" /></a></li>
      <li><a href="#"><img src="img/clientes/sebrae.png" alt="" /></a></li>
      <li><a href="#"><img src="img/clientes/iesb.png" alt="" /></a></li>
    </ul>
    <a href="#" class="btn">Tenho Interesse</a>
  </div>

</div>
<?php include 'includes/footer.php' ?>
