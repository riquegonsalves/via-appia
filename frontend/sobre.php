
	<?php include 'includes/head.php' ?>
	<?php include 'includes/header.php' ?>
  <div class="title-banner">
  	<div class="container">
  		<h1>SOBRE VIA APPIA</h1>
			<h2>Quem somos e o que fazemos</h2>
  	</div>
		<div class="seta">
			<div class="circle">
				<span>&nbsp;</span>
			</div>
		</div>
  </div>


	<div class="page page-sobre">
		<div class="title">

		</div>
		<div class="container">
			<div class="text">

				<h1>O que fazemos?</h1>
				<p>A Via Appia é uma empresa de tecnologia da informação totalmente voltada para a área da Ciência da Informação, desenvolvendo e comercializando produtos que utilizam em seu escopo soluções de busca, taxonomia, ontologia e thesauri multilíngues e poli-hierárquicos. A Empresa também fornece serviços específicos de tratamento de documentos bibliográficos ou arquivísticos, como processamento técnico, higienização, preparação e digitalização nos sistemas fornecidos ou de terceiros. Aliado a isso, vem fornecendo a plataforma de busca IBM Watson Explorer e implementando projetos com a utilização da mesma.</p>
			</div>
			<div class="img">
				<img src="img/sobre.jpg" alt="" />
			</div>
		</div>
		<div class="frase">
				<div class="container">
					<h2>“O modo como você reúne, administra e usa a informação determina se vencerá ou perderá.”</h2>
					<h4>- Bill Gates</h4>
				</div>
		</div>
		<div class="container">
			<h3>Produtos e Serviços</h3>
			<ul class="produtos">
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
			</ul>
			<a href="#" class="all-btn">
				Ver todos
			</a>

		</div>
		<div class="wrap-parceiros">
			<div class="container">
				<ul>
					<li>PARCEIROS:</li>
					<li><a href="#"><img src="img/parceiros/ibm.png" alt="" /></a></li>
					<li><a href="#"><img src="img/parceiros/hp.png" alt="" /></a></li>
					<li><a href="#"><img src="img/parceiros/ibm.png" alt="" /></a></li>
					<li><a href="#"><img src="img/parceiros/hp.png" alt="" /></a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<ul class="clientes">
				<li>CLIENTES:</li>
				<li><a href="#"><img src="img/clientes/ceub.png" alt="" /></a></li>
				<li><a href="#"><img src="img/clientes/bc.png" alt="" /></a></li>
				<li><a href="#"><img src="img/clientes/sebrae.png" alt="" /></a></li>
				<li><a href="#"><img src="img/clientes/iesb.png" alt="" /></a></li>
			</ul>
		</div>
	</div>


	<?php include 'includes/footer.php' ?>
