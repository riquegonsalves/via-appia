<?php include 'includes/head.php' ?>
<?php include 'includes/header.php' ?>
<div class="title-banner">
  <div class="container">
      <a href="#" class="back-button"><i class="back"></i> VOLTAR</a>
    <h1 class="title-segmento">THE XML</h1>
    <h2>produtos e serviços que compõem o roll de soluções da Via Appia</h2>
    <a href="#" class="button">Tenho Interesse</a>
  </div>

  <div class="seta seta-segmento">
    <div class="circle">
      <span>&nbsp;</span>
    </div>
  </div>
</div>

<div class="page page-product">
  <div class="container screenbg">
    <h1>PARA QUE SERVE?</h1>
    <div class="scroll-arrows">

    </div>

    <div class="screen">
      <div class="black" id="blackboard">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
    </div>
    <div class="screenBase">

    </div>
  </div>
  <div class="qualidade">
    <div class="container">
      <h1>ALTO PADRÃO DE QUALIDADE</h1>
      <p>Curabitur nisi. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Vivamus consectetuer hendrerit lacus. Praesent ac sem eget est egestas volutpat. Nunc nonummy metus.</p>
      <ul class="selos">
        <li>
          <img src="img/seloiso.png" alt="ISO 9001" />
        </li>
        <li>
          <img src="img/seloiso.png" alt="ISO 9001" />
        </li>
        <li>
          <img src="img/seloiso.png" alt="ISO 9001" />
        </li>
      </ul>
      <div class="scroll-arrows">

      </div>
    </div>
  </div>
  <div class="container">
    <h1>ESPECIFICAÇÕES OPERACIONAIS</h1>
    <ul class="especificacoes">
      <li>
        <div class="check disabled">

        </div>
        <div class="icon">
          <img src="img/especificacoes/navegadores.png" alt="" />
          <h2>Navegadores</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>
      </li>
      <li>
        <div class="check disabled">

        </div>
        <div class="icon">
          <img src="img/especificacoes/so.png" alt="" />
          <h2>Sistema Operacionais</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>
      </li>
      <li>
        <div class="check disabled">

        </div>
        <div class="icon">
          <img src="img/especificacoes/bd.png" alt="" />
          <h2>Banco de Dados</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>
      </li>
      <li>
        <div class="check disabled">

        </div>
        <div class="icon">
          <img src="img/especificacoes/webservices.png" alt="" />
          <h2>Webservices</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>
      </li>

    </ul>

    <a href="#" class="button">Tenho Interesse</a>
  </div>
</div>
<?php include 'includes/footer.php' ?>
