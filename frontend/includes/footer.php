<div class="footer">
	<div class="container">
		<div class="brasil">

		</div>
	</div>
	<footer>
		<div class="container">
			<div class="quarto">
				<div class="logo">
					<img src="img/logo.png" alt="" />
				</div>
				<ul>
					<li>
						<i class="mail"></i> comercial@viaappia.com.br
					</li>
					<li>
						<i class="mail"></i> seac@viaappia.com.br
					</li>
					<li>
						<i class="mail"></i> suporte@viaappia.com.br
					</li>
				</ul>
			</div>
			<div class="quarto">
				<h2>BRASIL <span>(MATRIZ)</span></h2>
				<div class="info">
					<div class="icon">
						<i class="icon-tel"></i>
					</div>
					<div class="text">
						<p>
							+55 61 33612750
						</p>
					</div>

				</div>
				<div class="info">
					<div class="icon">
						<i class="icon-pin"></i>

					</div>
					<div class="text">
						<p>Sia/Sul - Quadra 4c - Número 56</p>
						<p>Salas 201/203</p>
						<p>CEP: 71200.045 - Brasília/DF</p>
					</div>
				</div>
			</div>
			<div class="quarto">
				<h2>ESPANHA</h2>
				<div class="info">
					<div class="icon">
						<i class="icon-tel">

						</i>
					</div>
					<div class="text">
						<p>
							+34 911827885
						</p>
					</div>

				</div>
				<div class="info">
					<div class="icon">
						<i class="icon-pin">

						</i>

					</div>
					<div class="text">
						<p>Calle San Vicente, 16-1D</p>
						<p>28100 – Alcobendas</p>
					</div>
				</div>
			</div>
			<div class="quarto">
				<h2>PORTUGAL</h2>
				<div class="info">
					<div class="icon">
						<i class="icon-tel">

						</i>
					</div>
					<div class="text">
						<p>
							+351 302096576
						</p>
					</div>

				</div>
				<div class="info">
					<div class="icon">
						<i class="icon-pin">

						</i>

					</div>
					<div class="text">
						<p>Av das descobertas, 37-5D</p>
						<p>cp 2670-384 - Infantado</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>
<div class="copyright">
	<p>
		Via Appia © 2016 . Todos os direitos reservados. Desenvolvido em parceria com <a href="http://www.farolsoftware.com" target="_blank">Farol Software</a>
	</p>
</div>

<div class="modal faleConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Fale Conosco</h2>
			<div class="divisao">

			</div>
			<p>
				Podemos ajudar de alguma forma? Entre em contato por telefone, por email ou pelo formulário de contato abaixo. Retornamos dentro de 24h.
			</p>
			<ul>
				<li>• contato@pousadacamelot.com.br</li>
				<li>• (62)3333-9999 | 3333-9999</li>
			</ul>

		</div>

		<div class="form">
			<form class="" action="index.html" method="post">
				<div class="fields">
					<input type="text" name="name" placeholder="nome*">
					<input type="text" name="phone" placeholder="telefone*">
					<input type="email" name="email" placeholder="email">
				</div>
				<div class="textarea">
					<textarea name="message" placeholder="mensagem*"></textarea>
					<button type="submit">ENVIAR</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="js/jquery.js"></script>
<script src="js/typed.js"></script>
<script src="js/app.js"></script>

</body>
</html>
