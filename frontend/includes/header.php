<header>
  <div class="container">
    <a href="#" class="hLogo">
      <img src="img/logo.png" alt="via appia" />
    </a>
    <nav>
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">Sobre</a></li>
        <li><a href="#">Segmento</a></li>
        <li><a href="#">Segmento</a></li>
        <li><a href="#">Segmento</a></li>
        <li><a href="#">Segmento</a></li>
        <li><a href="#">Artigos</a></li>
        <li><a href="#">Portifólio</a></li>
      </ul>
      <div class="languages">
        <div class="pt">
          <span>PT</span>
        </div>
        <div class="en">
          <span>EN</span>
        </div>
      </div>
    </nav>
    <div class="mobile-close">
    </div>
  </div>
</header>
