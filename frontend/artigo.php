<?php include 'includes/head.php' ?>
<?php include 'includes/header.php' ?>
<div class="article-banner">
  <div class="container">
    <a href="#" class="back-button"><i class="back"></i> VOLTAR</a>
    <h1>Las cuatro prioridades para las bibliotecas del futuro</h1>
    <div class="center">
      <div class="category">
        <span>Artigo</span>
      </div>
      <h4>por: Jualian Marquina</h4>
      <h4>29/05/2013</h4>
    </div>
  </div>
  <div class="seta">
    <div class="circle">
      <span>&nbsp;</span>
    </div>
  </div>
</div>
<div class="page page-article">
  <div class="container">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

    <h2>Título Secundário</h2>

    <ul>
      <li>• Curabitur nisl. Sed magna purus</li>
      <li>• Curabitur nisl. Sed magna purus</li>
      <li>• Curabitur nisl. Sed magna purus</li>
      <li>• Curabitur nisl. Sed magna purus</li>
    </ul>

    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

    <div class="firula">
      <img src="img/firulas.png" alt="" />
    </div>


  </div>

</div>
<?php include 'includes/footer.php' ?>
