
	<?php include 'includes/head.php' ?>
	<?php include 'includes/header.php' ?>
  <div class="title-banner">
  	<div class="container">
  		<h1>PORTFÓLIO</h1>
			<h2>produtos e serviços que compõem o roll de soluções da Via Appia</h2>
  	</div>
		<div class="seta">
			<div class="circle">
				<span>&nbsp;</span>
			</div>
		</div>
  </div>

	<div class="page page-portfolio">
		<div class="container">
			<ul class="produtos">
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="icon">
							<img src="img/portfolio/xml.png" alt="" />
						</div>
						<h2>The XML</h2>
						<p>
							indexação inteligente  de bancos da dos estruturados e não-estruturados
						</p>
						<div class="seta">
							<i class="icon-arrow"></i>
						</div>
					</a>
				</li>


			</ul>
			<div class="searchBox">
				<h3>SEARCH - IN - A - BOX</h3>
				<p>
					O dispositivo de appliance que organiza todos os seus arquivos e permitie buscas intuitivas para a análise inteligente dos dados da sua empresa. Tudo isso de forma simples e rápida!
				</p>
				<a href="#">
					Ver Mais
				</a>
			</div>
		</div>
	</div>


	<?php include 'includes/footer.php' ?>
