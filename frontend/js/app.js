//carousel segmento


var order = 0;
var total = $('.visible ul li').length;

console.log(total);

$('.arrow-nav .next').click(function(){

	if(order == total-1){
		order = 0;
		$('.visible ul').css('top', '0px');
	} else {
		order++;
		var place = order * -220;
		$('.visible ul').css('top', place+'px')
	}
});

$('.arrow-nav .prev').click(function(){

	if(order == 0){
		order = total-1;
		var place = order * -220;
		$('.visible ul').css('top', place+'px')

	} else {
		order--;
		var place = order * -220;
		$('.visible ul').css('top', place+'px')
	}
});




$('.l-fale').click(function(){
	$('.faleConosco').css('left', 0);
});
$('.l-trabalhe').click(function(){
	$('.trabalheConosco').css('left', 0);
});

$('.modal .close-pi').click(function(){
	$('.modal').css('left', '-100vw');
});

$(document).mouseup(function (e)
{
    var container = $(".modal .container");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {


				$('.modal').css('left', '-100vw');
    }
});

$('.mobile-head .menu-link, .mobile-close').click(function(){
	$('header').toggleClass('aberto');
});

var mq = window.matchMedia( "(min-width: 860px)" );

if(mq.matches){

}


$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

if($('.siab-banner img').length){
	$('.siab-banner img').animateCss('bounce');
}


var um = 0;
var dois = 0;
var tres = 0;
$(window).on('scroll', function(){
	var s = $(window).scrollTop();

	if($('.especificacoes').length){
		if(s >= $('.especificacoes').offset().top - 300){
			$('.especificacoes li:first-of-type .check').removeClass('disabled');
			setTimeout(function(){
				$('.especificacoes li:nth-of-type(2) .check').removeClass('disabled');
				setTimeout(function(){
					$('.especificacoes li:nth-of-type(3) .check').removeClass('disabled');
					setTimeout(function(){
						$('.especificacoes li:nth-of-type(4) .check').removeClass('disabled');
					}, 500);
				}, 500);
			}, 500);
		} 
	}


	if($('.essencial').length){
		if(s >= $('.essencial').offset().top - 300){
			$('.num').addClass('open');
		} else {
			$('.num').removeClass('open');

		}
	}

	if($('.screenbg').length){

		var screenbg = $('.screenbg').offset().top;

		if(s >= screenbg){
			var n = s - screenbg;

			$('.screen').css('margin-top', n+'px')

			var point1 = 300;
			var point2 = 600;
			var point3 = 900;

			if(n >= point1 && n <= point2){

				if(um == 0){

					$("#blackboard").typed({
						strings: ["Agora."],
						typeSpeed: 0
					});
				}

				um = 1;
				dois = 0;
				tres= 0;

			}

			if(n >= point2 && n <= point3){

				if(dois == 0){
					$("#blackboard").typed({
						strings: ["Agora2."],
						typeSpeed: 0
					});
				}

				um = 0;
				dois = 1;
				tres = 0;
			}

			if(n >= point3){

				if(tres == 0){
					$("#blackboard").typed({
						strings: ["Agora3."],
						typeSpeed: 0
					});
				}

				dois = 0;
				tres = 1;

			}
		}
	}

	if($('.about img').length){
		var aboutImg = $('.about img').offset().top;
		if(s > aboutImg-400){
			$('.about img').addClass('scaled');
			$('.about .text').addClass('open');
		} else {
			$('.about img').removeClass('scaled');
			$('.about .text').removeClass('open');
		}
	}

	if($('.perfect .text').length){
		var perfectText = $('.perfect .text').offset().top;
		if(s > perfectText-400){
			$('.perfect .text').addClass('open');
			$('.perfect .img').addClass('open');

		} else {
			$('.perfect .text').removeClass('open');
			$('.perfect .img').removeClass('open');
		}
	}

	if($('.configuration h2').length){
		var configTitle = $('.configuration h2').offset().top;

		if(s > configTitle-400){
			$('.configuration .left .info, .configuration .right .info').fadeIn(300);
		} else {
			$('.configuration .left .info, .configuration .right .info').fadeOut(300);
		}
	}

});
