<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php global $post; ?>
<?php $banner = get_field('bannerpromocional', $post->ID); ?>
<?php $image_banner = get_field('imagem', $banner); ?>

<?php if($banner): ?>
	<div class="banner-promocional">
		<div class="container">
			<?php if($image_banner): ?>
				<div class="searchBox" style="background: url('<?= $image_banner ?>')">
			<?php else: ?>
				<div class="searchBox">
			<?php endif; ?>
				<h3><?= $banner->post_title; ?></h3>
				<?= $banner->post_content; ?>

				<?php $produto = get_field('produto', $banner->ID); ?>

				<?php if($produto): ?>
					<a href="<?= get_permalink($produto->ID) ?>"> Ver Mais </a>
				<?php else: ?>
					<a href="<?= get_field('link', $banner->ID) ?>"> Ver Mais </a>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if((get_post_type($post)) == "produto"): ?>
	<?php $tituloProduto = get_the_title($post); ?>
	<?php $detalhes_tecnicos = get_field('detalhes_tecnicos', get_the_id()); ?>
<? endif; ?>

<div class="modal formDetalhes">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Detalhes: <?= $tituloProduto ?></h2>

			<div class="divisao">
			</div>

			<div class="div-detalhes">
				<?= $detalhes_tecnicos ?>
			</div>
			<a href="#" class="buttonModal button-modal">Tenho Interesse</a>

		</div>

	</div>
</div>

<div class="footer">
	<div class="container">
		<div class="brasil">

		</div>
	</div>
	<footer>
		<div class="container">
			<div class="quarto">
				<div class="logo">
					<img src="<?php bloginfo('template_url')?>/img/logo.png" alt="" />
				</div>
				<ul class="sendMail">
					<li>
						<a href="mailto:comercial@viaappia.com.br">
							<i class="mail"></i> comercial@viaappia.com.br
						</a>
					</li>
					<li>
						<a href="mailto:seac@viaappia.com.br">
							<i class="mail"></i> seac@viaappia.com.br
						</a>
					</li>
					<li>
						<a href="mailto:suporte@viaappia.com.br">
							<i class="mail"></i> suporte@viaappia.com.br
						</a>
					</li>
					<li>
						<a href="#">
							<p class="l-trabalhe">• Trabalhe Conosco</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="quarto">
				<h2>BRASIL <span>(MATRIZ)</span></h2>
				<div class="info">
					<a href="tel:+556133612750">
						<div class="icon">
							<i class="icon-tel"></i>
						</div>
						<div class="text">
							<p>
								+55 61 33612750
							</p>
						</div>
					</a>

				</div>
				<div class="info">
					<a href="https://goo.gl/maps/jo4jGEprJyL2" target="_blank">
						<div class="icon">
							<i class="icon-pin"></i>

						</div>
						<div class="text">
							<p>Sia/Sul - Quadra 4c - Número 56</p>
							<p>Salas 201/203</p>
							<p>CEP: 71200.045 - Brasília/DF</p>
						</div>
					</a>
				</div>
			</div>
			<div class="quarto">
				<h2>ESPANHA</h2>
				<div class="info">
					<a href="tel:+34911827885">

						<div class="icon">
							<i class="icon-tel">

							</i>
						</div>
						<div class="text">
							<p>
								+34 911827885
							</p>
						</div>
					</a>

				</div>
				<div class="info">
					<a href="https://goo.gl/maps/6vh58GqZSVR2" target="_blank">
						<div class="icon">
							<i class="icon-pin">

							</i>

						</div>
						<div class="text">
							<p>Calle San Vicente, 16-1D</p>
							<p>28100 – Alcobendas</p>
						</div>
					</a>
				</div>
			</div>
			<div class="quarto">
				<h2>PORTUGAL</h2>
				<div class="info">
					<a href="tel:+351302096576">
						<div class="icon">
							<i class="icon-tel">

							</i>
						</div>
						<div class="text">
							<p>
								+351 302096576
							</p>
						</div>
					</a>

				</div>
				<div class="info">
					<a href="https://goo.gl/maps/V6k2n1zHgR32" target="_blank">
						<div class="icon">
							<i class="icon-pin">

							</i>

						</div>
						<div class="text">
							<p>Av das descobertas, 37-5D</p>
							<p>cp 2670-384 - Infantado</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</footer>
</div>
<div class="copyright">
	<p>
		Via Appia © 2016 . Todos os direitos reservados. Desenvolvido em parceria com <a href="http://www.farolsoftware.com" target="_blank">Farol Software</a>
	</p>
</div>

<div class="modal trabalheConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Trabalhe Conosco</h2>
			<div class="divisao">

			</div>
			<p>Quer fazer parte da nossa equipe, aproveitar as oportunidades e crescer junto com a gente? Estamos sempre em busca de pessoas motivadas e dispostas a colocar a mão na massa! Afinal, é com bastante empenho e disposição que a gente consegue oferecer um ótimo serviço para os nossos clientes!</p>
			<p>Informe os seus dados abaixo, anexe seu currículo e entraremos em contato!</p>

		</div>

		<div class="form">
			<form class="" id="formTrabalhe">
				<div class="fields">
					<div class="field">
						<input type="text" name="name" class="required" placeholder="nome*">
					</div>
					<div class="field">
						<input type="text" name="phone" class="required" placeholder="telefone*">
					</div>
					<div class="field">
						<input type="email" name="email" placeholder="email">
					</div>
				</div>
				<div class="textarea">
					<textarea name="message" class="required" placeholder="mensagem*"></textarea>

				</div>
        <div class="fields fieldsCV">
          <div class="field">

            <button type="submit">ENVIAR</button>
            <label for="cv" class="labelFile">Anexar currículo</label>
            <input type="file" name="cv" id="cvFile">

          </div>


        </div>
			</form>
		</div>
	</div>
</div>



<div class="modal faleConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<?php
			 $currentlang = get_bloginfo('language');
			 if($currentlang=="en-US"):
			?>
				<h2>I'm Interested</h2>
				<div class="divisao">

				</div>
				<p>We can help in any way? Contact by phone, email or contact form below . We returned within 24 hours.</p>
			<?php else: ?>
				<h2>Tenho interesse</h2>
				<div class="divisao">

				</div>
				<p>
					Podemos ajudar de alguma forma? Entre em contato por telefone, por email ou pelo formulário de contato abaixo. Retornamos dentro de 24h.
				</p>

				<ul>
					<li>• comercial@viaappia.com.br</li>
					<li>• (62)3333-9999 | 3333-9999</li>
				</ul>
			<?php endif; ?>



		</div>

		<div class="form">
			<form class="" id="formFale">
				<?php
				 $currentlang = get_bloginfo('language');
				 if($currentlang=="en-US"):
				?>
					<div class="fields">
						<div class="field">
							<input type="text" class="required" name="name" placeholder="name*">
						</div>
						<div class="field">
							<input type="text" class="required" name="phone" placeholder="phone*">
						</div>
						<div class="field">
							<input type="email" class="required email" name="email" placeholder="email">
						</div>
						<input type="hidden" name="type" value="">
					</div>
					<div class="textarea">
						<textarea name="message" placeholder="message*" class="required"></textarea>
						<button type="submit">SEND</button>
					</div>
				<?php else: ?>
					<div class="fields">
						<div class="field">
							<input type="text" class="required" name="name" placeholder="nome*">
						</div>
						<div class="field">
							<input type="text" class="required" name="phone" placeholder="telefone*">
						</div>
						<div class="field">
							<input type="email" class="required email" name="email" placeholder="email">
						</div>
						<input type="hidden" name="type" value="">
					</div>
					<div class="textarea">
						<textarea name="message" placeholder="mensagem*" class="required"></textarea>
						<button type="submit">ENVIAR</button>
					</div>
				<?php endif; ?>
			</form>
		</div>
	</div>
</div>


<script src="<?php bloginfo('template_url') ?>/js/jquery.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/validate.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/typed.js"></script>
<script>
  var siteURL = "<?php bloginfo('url') ?>";
</script>
<script src="<?php bloginfo('template_url') ?>/js/app.js"></script>



<?php wp_footer(); ?>
</body>
</html>
