<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<div class="title-banner">
<div class="container">
<h1>ARTIGOS E NOTÍCIAS</h1>
<h2><?php printf( __( 'Posts da Categoria: %s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h2>
</div>
<div class="seta">
<div class="circle">
<span>&nbsp;</span>
</div>
</div>
</div>
<div class="page page-articles">
<div class="container">
	<?php if(have_posts()): ?>
		<ul>
			<?php while (have_posts()) : the_post(); ?>
				<li>
					<a href="<?= get_permalink(); ?>">
						<div class="text">
							<div class="category">
								<?php
										$post_categories = wp_get_post_categories(get_the_ID());

										foreach($post_categories as $c){
											$cat = get_category( $c ); ?>


														<span><?php echo $cat->name; ?></span>

									<?php

										}

									?>
							</div>
							<h2><?php the_title() ?></h2>
							<h4>por: <?php the_author(); ?></h4>
							<h4><?php the_date('d/m/Y'); ?></h4>

						</div>


					</a>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</div>
</div>





<?php

get_footer();
