<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link href="<?php bloginfo('template_url') ?>/img/favicon.ico" rel="icon" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<meta charset="utf-8">
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Via Appia</title>
	<link href="<?php bloginfo('template_url') ?>/stylesheets/animate.min.css" rel="stylesheet" />
	<link href="<?php bloginfo('template_url') ?>/stylesheets/screen.css" rel="stylesheet" />
	<link href='https://fonts.googleapis.com/css?family=Oswald:400,700|Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

</head>


<body <?php body_class(); ?>>
	<div class="wrapper">
		<div class="mobile-head">
			<div class="container">
				<div class="menu-link">

				</div>
				<img src="<?php bloginfo('template_url')?>/img/logo.png" alt="" class="mobile-logo"/>
			</div>
		</div>

		<section class="mobileFixed">
			<div class="container">

			</div>
		</section>
	</div>
	<header>
	  <div class="container">
			<?php
			 $currentlang = get_bloginfo('language');
			 if($currentlang=="en-US"):
			?>
	    	<a href="/en/" class="hLogo">
			<?php else: ?>
				<a href="/" class="hLogo">
			<?php endif; ?>
	      <img src="<?php bloginfo('template_url')?>/img/logo.png" alt="via appia" />
	    </a>
			<style>
				.menu-principal-container{
					display: inline-block;
				}
			</style>
	    <nav>
	      <?php wp_nav_menu (array('theme_location' => 'primary-menu','menu_class' => 'nav'));?>
	      <div class="languages">
					<a href="/">
						<div class="pt">
		          <span>PT</span>
		        </div>
					</a>

	        <a href="/en/">
						<div class="en">
		          <span>EN</span>
		        </div>
	        </a>
	      </div>
	    </nav>
	    <div class="mobile-close">
	    </div>
	  </div>
	</header>
