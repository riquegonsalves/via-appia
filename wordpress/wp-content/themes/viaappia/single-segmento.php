<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


		<?php

		while ( have_posts() ) : the_post(); ?>
    <?php
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    ?>

		<div class="title-banner title-banner-alt">
		  <div class="container">
		      <a href="#" class="back-button"></a>
		    <h1 class="title-segmento"><?php the_title() ?></h1>
		    <h2><?php the_content(); ?></h2>
		    <a href="#" class="button">Tenho Interesse</a>
		  </div>

		  <div class="seta seta-segmento">
		    <div class="circle">
		      <span>&nbsp;</span>
		    </div>
		  </div>
		</div>

		<div class="page page-segmento">
		  <div class="container screenbg">
		    <h1>PARA QUE SERVE?</h1>
		    <div class="scroll-arrows scrollone">

		    </div>


				<div class="hiddeninfo" style="display: none">

					<?php if(get_field('pra_que_serve')): ?>


						<?php $contador = 1; while(has_sub_field('pra_que_serve')): ?>

							<?php
								if($contador == 1){
									$texto = get_sub_field('texto');
								}
							?>

							<div class="info-<?= $contador ?>"><?php the_sub_field('texto'); ?></div>

						<?php $contador++; endwhile; ?>


					<?php endif; ?>

				</div>


				<div class="mobileScreen">
					
					<div class="screen">
			      <div class="black">
							<?php if(get_field('pra_que_serve')): ?>


						<?php $contador = 1; while(has_sub_field('pra_que_serve')): ?>


							<p><?php the_sub_field('texto'); ?></p>

						<?php $contador++; endwhile; ?>


					<?php endif; ?>

			      </div>
			    </div>

			    <div class="screenBase">
			    </div>
				</div>
				<div class="bigScreen">
				<div class="screen">
					<div class="screenArrowSegmentoUp">
						
					</div>
			      <div class="black" id="blackboard">
			      </div>
			    </div>
			    <div class="screenBase">
			    </div>
				</div>
			    <div class="screenArrowSegmento">
			    </div>

		  </div>
			<div class="container">
				<div class="essencial">
					<h1>ESSENCIAL PARA A SUA EMPRESA</h1>

					<?php if(get_field('essencial')): ?>
						<ul>
							<?php $contador = 1; while(has_sub_field('essencial')): ?>

								<li>
									<div class="num"><?= $contador ?></div>
									<p><?php the_sub_field('texto'); ?></p>
								</li>

							<?php $contador++; endwhile; ?>
						</ul>
					<?php endif; ?>


					<a href="#" class="solucoesBtn">Veja nossas soluções</a>
				</div>
				<div class="solutions">
					<h1 style="text-transform: uppercase">NOSSAS SOLUÇÕES EM <?php the_title() ?> </h1>
					<div class="bigScreen">
						<div class="carousel">
							<div class="visible">

								<?php if(get_field('produtos')): ?>
									<ul>
										<?php while(has_sub_field('produtos')): ?>

											<li>
												<?php
													$produto = get_sub_field('produto');
													$content_post = get_post($produto->ID);
													$content = $content_post->post_content;
													$content = apply_filters('the_content', $content);
													$content = str_replace(']]>', ']]&gt;', $content);

												?>
												<a href="<?= get_post_permalink($produto->ID) ?>">
													<div class="main">
														<div class="title">

															<div class="icon">
																<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($produto->ID) ) ?>" alt="" />
															</div>
															<h3 style="text-align: center; margin: 0	"><?= get_the_title($produto->ID) ?></h3>

														</div>
														<div class="text">
															<p>
																<?= $content ?>
															</p>
														</div>
													</div>
													<a href="<?= get_post_permalink($produto->ID) ?>" class="more">
														saber mais...
													</a>
												</a>

											</li>

											<?php endwhile; ?>
										</ul>
									<?php endif; ?>
							</div>
							<div class="arrow-nav">
								<div class="prev active">

								</div>
								<div class="next active">

								</div>
							</div>
						</div>
					</div>
					<div class="mobileScreen">
						<?php if(get_field('produtos')): ?>
									<ul>
										<?php while(has_sub_field('produtos')): ?>

											<li>
												<?php
													$produto = get_sub_field('produto');
													$content_post = get_post($produto->ID);
													$content = $content_post->post_content;
													$content = apply_filters('the_content', $content);
													$content = str_replace(']]>', ']]&gt;', $content);

												?>
												<a href="<?= get_post_permalink($produto->ID) ?>">
													<div class="main">
														<div class="title">

															<div class="icon">
																<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($produto->ID) ) ?>" alt="" />
															</div>
															<h3 style="text-align: center; margin: 0	"><?= get_the_title($produto->ID) ?></h3>

														</div>
														<div class="text">
															<p>
																<?= $content ?>
															</p>
														</div>
													</div>
													<a href="<?= get_post_permalink($produto->ID) ?>" class="more">
														saber mais...
													</a>
												</a>

											</li>

											<?php endwhile; ?>
										</ul>
									<?php endif; ?>
					</div>
					<a href="#" class="btn">Tenho Interesse</a>
				</div>
			</div>
		  <div class="qualidade">
		    <div class="container">
		      <h1>ALTO PADRÃO DE QUALIDADE</h1>
		      <p><?= get_field('qualidade') ?></p>
					<?php if(get_field('certificados_de_qualidade')): ?>
			      <ul class="selos">
							<?php while(has_sub_field('certificados_de_qualidade')): ?>


								<li><img src="<?php the_sub_field('certificado'); ?>" /></li>

							<?php endwhile; ?>
			      </ul>
					<?php endif; ?>
		      <div class="scroll-arrows scrolltwosegmento">

		      </div>
		    </div>
		  </div>
			<div class="container">
				<?php include 'includes/clientes.php' ?>
			</div>

		</div>


    <?php
		  endwhile;	?>

<?php get_footer(); ?>
