<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


		<?php

		while ( have_posts() ) : the_post(); ?>
    <?php
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    ?>

		<div class="title-banner title-banner-alt">
		  <div class="container">
		      <a href="#" class="back-button"></a>
		    <h1 class="title-segmento"><?php the_title() ?></h1>
		    <h2><?php the_content(); ?></h2>
		    <a href="#" class="button">Tenho Interesse</a>
		  </div>

		  <div class="seta seta-segmento">
		    <div class="circle">
		      <span>&nbsp;</span>
		    </div>
		  </div>
		</div>

		<div class="page page-product">
		  <div class="container screenbg">
		    <h1>PARA QUE SERVE?</h1>
		    <div class="scroll-arrows scrollone">

		    </div>


				<div class="hiddeninfo" style="display: none">

					<?php if(get_field('pra_que_serve')): ?>


						<?php $contador = 1; while(has_sub_field('pra_que_serve')): ?>

							<?php
								if($contador == 1){
									$texto = get_sub_field('texto');
								}
							?>

							<div class="info-<?= $contador ?>"><?php the_sub_field('texto'); ?></div>

						<?php $contador++; endwhile; ?>


					<?php endif; ?>

				</div>
				<div class="mobileScreen">
					
					<div class="screen">
			      <div class="black">
							<?php if(get_field('pra_que_serve')): ?>


						<?php $contador = 1; while(has_sub_field('pra_que_serve')): ?>


							<p><?php the_sub_field('texto'); ?></p>

						<?php $contador++; endwhile; ?>


					<?php endif; ?>

			      </div>
			    </div>
			    <div class="screenBase">

			    </div>
				</div>

				<div class="bigScreen">
				<div class="screen">
					<div class="screenArrowProdutoUp">
					</div>
					<div class="black" id="blackboard">
					</div>
			    </div>
			    <div class="screenBase">
			    </div>
				</div>
			    <div class="screenArrowProduto">
			    </div>

		  </div>
		  <div class="qualidade">
		    <div class="container">
		      <h1>ALTO PADRÃO DE QUALIDADE</h1>
		      <p><?= get_field('qualidade') ?></p>
					<?php if(get_field('certificados_de_qualidade')): ?>
			      <ul class="selos">
							<?php while(has_sub_field('certificados_de_qualidade')): ?>


								<li><img src="<?php the_sub_field('certificado'); ?>" /></li>

							<?php endwhile; ?>
			      </ul>
					<?php endif; ?>
		      <div class="scroll-arrows scrolltwoproduto">

		      </div>
		    </div>
		  </div>
		  <div class="container">
		    <h1>ESPECIFICAÇÕES OPERACIONAIS</h1>

				<?php if(get_field('especificacoes')): ?>
					<ul class="especificacoes">
						<?php while(has_sub_field('especificacoes')): ?>


							<li>
								<div class="check disabled">

								</div>
								<div class="icon">
									<img src="<?php the_sub_field('icone'); ?>" />
									<h2><?php the_sub_field('nome'); ?></h2>
									<p>
										<?php the_sub_field('texto'); ?>
									</p>
								</div>

							</li>

						<?php endwhile; ?>
					</ul>
				<?php endif; ?>


		    <a href="#" class="button">Tenho Interesse</a>
		    <div class="container container-mais-detalhes">
			    <a class="mais-detalhes">+ Mais Detalhes do Produto</a>
		    </div>
		  </div>
		</div>


    <?php
		  endwhile;	?>

<?php get_footer(); ?>
