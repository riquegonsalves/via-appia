if($('#preloader').length){
	$(window).load(function(){
		$('#status').fadeOut();
		$('#logostatus').fadeOut();
		$('#preloader').delay(1000).fadeOut("slow");
	});
}


if($('.page-home').length){
	$('.circle').click(function(){
		$('html, body').animate({scrollTop: ($(window).scrollTop() + 300) + 'px'}, 500);
		event.preventDefault();
	});	
}

if($('.page-portfolio').length){
	$('.circle').click(function(){
		$('html, body').animate({scrollTop: ($(window).scrollTop() + 470) + 'px'}, 500);
		event.preventDefault();
	});	
}

if($('.page-siab').length){
	$('.circle').click(function(){
		$('html, body').animate({scrollTop: $(".about").offset().top}, 500);		
		event.preventDefault();
	});	
}

if($('.page-siab').length || $('.single-produto').length){
	$('.button, .btn').click(function(){
		$('.modal.faleConosco').css('left', 0);
		var segmento = $('.title-segmento').html();
		$('.modal.faleConosco input[name="type"]').val(segmento);
		event.preventDefault();
	});
}

if($('.single-segmento').length || $('.single-produto').length){
	$('.button, .btn').click(function(){
		$('.modal.faleConosco').css('left', 0);
		var segmento = 'searchinabox';
		$('.modal.faleConosco input[name="type"]').val(segmento);
		event.preventDefault();
	});

	$('.solucoesBtn').click(function(){
		$('html, body').animate({scrollTop: $(".solutions").offset().top - 50}, 500);
		event.preventDefault();
	});

	$('.scrollone').click(function(){
		$('html, body').animate({scrollTop: $(".bigScreen .screen").offset().top - 50}, 500);
		event.preventDefault();
	});

	$('.scrolltwoproduto').click(function(){
		$('html, body').animate({scrollTop: $(".especificacoes").offset().top - 150}, 500);
		event.preventDefault();
	});

	$('.scrolltwosegmento').click(function(){
		$('html, body').animate({scrollTop: $(".clientes").offset().top - 50}, 500);
		event.preventDefault();
	});

	$('.screenArrow').click(function(){
		if($(window).scrollTop() > 1688){
			$('html, body').animate({scrollTop: $(".qualidade").offset().top}, 500);
			event.preventDefault();
		}else{
			$('html, body').animate({scrollTop: ($(window).scrollTop() + 550) + 'px'}, 500);
			event.preventDefault();
		}
	});

	$('.screenArrowSegmento').click(function(){
		if(($(window).scrollTop() >= 616) && ($(window).scrollTop() <= 1075)){
			$('html, body').animate({scrollTop: '1462px'}, 500);
			event.preventDefault();
		}else if(($(window).scrollTop() >= 1076) && ($(window).scrollTop() <= 1675)){
			$('html, body').animate({scrollTop: '1941px'}, 500);
			event.preventDefault();
		}else if($(window).scrollTop() >= 1077){
			$('html, body').animate({scrollTop: $(".essencial").offset().top - 190}, 500);
			event.preventDefault();
		}
	});

	$('.screenArrowSegmentoUp').click(function(){
		if(($(window).scrollTop() >= 616) && ($(window).scrollTop() <= 1075)){
			$('html, body').animate({scrollTop: '0px'}, 500);
			event.preventDefault();
		}else if(($(window).scrollTop() >= 1076) && ($(window).scrollTop() <= 1675)){
			$('html, body').animate({scrollTop: '759px'}, 500);
			event.preventDefault();
		}else if($(window).scrollTop() >= 1077){
			$('html, body').animate({scrollTop: '1462px'}, 500);
			event.preventDefault();
		}
	});

	$('.screenArrowProduto').click(function(){
		if(($(window).scrollTop() >= 616) && ($(window).scrollTop() <= 1075)){
			$('html, body').animate({scrollTop: '1462px'}, 500);
			event.preventDefault();
		}else if(($(window).scrollTop() >= 1076) && ($(window).scrollTop() <= 1675)){
			$('html, body').animate({scrollTop: '1941px'}, 500);
			event.preventDefault();
		}else if($(window).scrollTop() >= 1077){
			$('html, body').animate({scrollTop: $(".qualidade").offset().top}, 500);
			event.preventDefault();
		}
	});

	$('.screenArrowProdutoUp').click(function(){
		if(($(window).scrollTop() >= 616) && ($(window).scrollTop() <= 1075)){
			$('html, body').animate({scrollTop: '0px'}, 500);
			event.preventDefault();
		}else if(($(window).scrollTop() >= 1076) && ($(window).scrollTop() <= 1675)){
			$('html, body').animate({scrollTop: '759px'}, 500);
			event.preventDefault();
		}else if($(window).scrollTop() >= 1077){
			$('html, body').animate({scrollTop: '1462px'}, 500);
			event.preventDefault();
		}
	});

	$('.circle').click(function(){
		$('html, body').animate({scrollTop: ($(window).scrollTop() + 700) + 'px'}, 500);
		event.preventDefault();
	});	

}

if($('.back-button').length){
	$('.back-button').click(function(){
		window.history.back();
		event.preventDefault();
	});
}



var order = 0;
var total = $('.visible ul li').length;

console.log(total);

$('.arrow-nav .next').click(function(){

	if(order == total-1){
		order = 0;
		$('.visible ul').css('top', '0px');
	} else {
		order++;
		var place = order * -220;
		$('.visible ul').css('top', place+'px')
	}
});

$('.arrow-nav .prev').click(function(){

	if(order == 0){
		order = total-1;
		var place = order * -220;
		$('.visible ul').css('top', place+'px')

	} else {
		order--;
		var place = order * -220;
		$('.visible ul').css('top', place+'px')
	}
});




$('.button-modal').click(function(){
	$('.modal').css('left', '-100vw');
	$('.faleConosco').css('left', 0);
	event.preventDefault();
});
$('.l-trabalhe').click(function(){
	$('.trabalheConosco').css('left', 0);
	event.preventDefault();
});
$('.mais-detalhes').click(function(){
	$('.formDetalhes').css('left', 0);
	event.preventDefault();
});

$('.modal .close-pi').click(function(){
	$('.modal').css('left', '-100vw');
});


$(function() {
  $(".labelFile").click(function() {
    $("#cvFile").trigger('click');
  });

  $('#cvFile').change(function(e){
    $('.labelFile').html($('#cvFile').val());
  });
});



$(document).mouseup(function (e)
{
    var container = $(".modal .container");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {


				$('.modal').css('left', '-100vw');
    }
});

$('.mobile-head .menu-link, .mobile-close').click(function(){
	$('header').toggleClass('aberto');
});




$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

if($('.siab-banner img').length){
	$('.siab-banner img').animateCss('bounce');
}


var um = 0;
var dois = 0;
var tres = 0;
$(window).on('scroll', function(){
	var s = $(window).scrollTop();

	console.log(s);

	if($('.screenArrowSegmentoUp').length){
		if(s > ($('.screenbg').offset().top + 100)){

			$('.screenArrowSegmentoUp').fadeIn(300);
		}else{
			$('.screenArrowSegmentoUp').fadeOut(300);
		}
	}

	if($('.screenArrowProdutoUp').length){
		if(s > ($('.screenbg').offset().top + 100)){

			$('.screenArrowProdutoUp').fadeIn(300);
		}else{
			$('.screenArrowProdutoUp').fadeOut(300);
		}
	}

	if($('.especificacoes').length){
		if(s >= $('.especificacoes').offset().top - 300){
			$('.especificacoes li:first-of-type .check').removeClass('disabled');
			setTimeout(function(){
				$('.especificacoes li:nth-of-type(2) .check').removeClass('disabled');
				setTimeout(function(){
					$('.especificacoes li:nth-of-type(3) .check').removeClass('disabled');
					setTimeout(function(){
						$('.especificacoes li:nth-of-type(4) .check').removeClass('disabled');
					}, 500);
				}, 500);
			}, 500);
		}
	}


	if($('.essencial').length){
		if(s >= $('.essencial').offset().top - 300){
			$('.num').addClass('open');
		} else {
			$('.num').removeClass('open');

		}
	}

	if($('.screenbg').length){

		var screenbg = $('.screenbg').offset().top;

		if(s >= screenbg){
			var n = s - screenbg;


			var mq = window.matchMedia( "(max-width: 414px)" );

			if(mq.matches){
				n = n-60;
			}

			$('.bigScreen .screen').css('margin-top', n+'px')

			var point1 = -600;
			var point2 = 600;
			var point3 = 1200;

			var string1 = $('.hiddeninfo .info-1').html();
			var string2 = $('.hiddeninfo .info-2').html();
			var string3 = $('.hiddeninfo .info-3').html();

			if(n >= point1 && n <= point2){

				if(um == 0){

					$("#blackboard").typed({
						strings: [string1],
						typeSpeed: 0
					});
				}

				um = 1;
				dois = 0;
				tres= 0;

			}

			if(n >= point2 && n <= point3){

				if(dois == 0){
					$("#blackboard").typed({
						strings: [string2],
						typeSpeed: 0
					});
				}

				um = 0;
				dois = 1;
				tres = 0;
			}

			if(n >= point3){

				if(tres == 0){
					$("#blackboard").typed({
						strings: [string3],
						typeSpeed: 0
					});
				}

				dois = 0;
				tres = 1;

			}
		}
	}

	if($('.about img').length){
		var aboutImg = $('.about img').offset().top;
		if(s > aboutImg-400){
			$('.about img').addClass('scaled');
			$('.about .text').addClass('open');
		} else {
			$('.about img').removeClass('scaled');
			$('.about .text').removeClass('open');
		}
	}

	if($('.perfect .text').length){
		var perfectText = $('.perfect .text').offset().top;
		if(s > perfectText-400){
			$('.perfect .text').addClass('open');
			$('.perfect .img').addClass('open');

		} else {
			$('.perfect .text').removeClass('open');
			$('.perfect .img').removeClass('open');
		}
	}

	if($('.configuration h2').length){
		var configTitle = $('.configuration h2').offset().top;

		if(s > configTitle-400){
			$('.configuration .left .info, .configuration .right .info').fadeIn(300);
		} else {
			$('.configuration .left .info, .configuration .right .info').fadeOut(300);
		}
	}

});


//Validacao de Formulario
$(document).ready(function(){
	$('#formTrabalhe').validate({
		submitHandler: function(form){

			var formulario = $('#formTrabalhe');
			var nome = $('#formTrabalhe input[name="name"]').val();
			var email = $('#formTrabalhe input[name="email"]').val();
			var fone = $('#formTrabalhe input[name="phone"]').val();
			var mensagem = $('#formTrabalhe textarea[name="message"]').val();
			var cv = $('#formTrabalhe input[name="cv"]').prop('files');

	    var formData = new FormData();
	    formData.append('nome', nome);
	    formData.append('action', 'contatoAction');
	    formData.append('email', email);
	    formData.append('fone', fone);
	    formData.append('mensagem', mensagem);
	    formData.append('tipo', 'Trabalhe Conosco');
	    formData.append('cv', $('#formTrabalhe input[name="cv"]')[0].files[0]);

	    for (var pair of formData.entries()) {
	      console.log(pair[0]+ ', ' + pair[1]);
	    }


			$.ajax({
				type: "POST",
				url: siteURL+"/wp-admin/admin-ajax.php",
				data: formData,
	      contentType: false,
	      processData: false,
				success:function(response){
					alert('Formulário enviado com sucesso!');
					$('#formTrabalhe button').html('Enviado com sucesso!');
					$('#formTrabalhe input,  #formFale textarea').val('');
					$('.modal.trabalheConosco').css('left', '100vw');

				}
			});
			return false;
		}
	});

	$('#formFale').validate({
	submitHandler: function(form){

		var formulario = $('#formFale');
		var nome = $('#formFale input[name="name"]').val();
		var email = $('#formFale input[name="email"]').val();
		var fone = $('#formFale input[name="phone"]').val();
		var mensagem = $('#formFale textarea[name="message"]').val();
		var tipo = $('#formFale input[name="type"]').val();



		$.ajax({
			type: "POST",
			url: siteURL+"/wp-admin/admin-ajax.php",
			data: {
				action: 'contatoAction',
				nome:nome,
				email:email,
				mensagem:mensagem,
				tipo: tipo
			},
			dataType: 'JSON',
			success:function(response){
				alert('Formulário enviado com sucesso!');
				$('#formFale button').html('Enviado com sucesso!');
				$('#formFale input,  #formFale textarea').val('');
				$('.modal.faleConosco').css('left', '100vw');

			},
			error:function(response){
				$('#formFale button').html('Erro ao enviar! Tente novamente');
				$('#formFale input,  #formFale textarea').val('');
			}
		});
		return false;
	}
});

});
