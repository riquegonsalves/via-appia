<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


		<?php

		while ( have_posts() ) : the_post(); ?>

		<div class="article-banner">
		  <div class="container">
		    <a href="#" class="back-button"></a>
		    <h1><?php the_title() ?></h1>
		    <div class="center">
		      <div class="category">
						<?php
								$post_categories = wp_get_post_categories(get_the_ID());

								foreach($post_categories as $c){
									$cat = get_category( $c ); ?>


								<a href="<?php bloginfo('url') ?>/category/<?php echo $cat->slug; ?>" class="categoria"><span><?php echo $cat->name; ?></span></a>

							<?php

								}

							?>
		      </div>
		      <h4>por: <?php the_author(); ?></h4>
		      <h4><?php the_date('d/m/Y'); ?></h4>
		    </div>
		  </div>
		  <div class="seta">
		    <div class="circle">
		      <span>&nbsp;</span>
		    </div>
		  </div>
		</div>
		<div class="page page-article">
		  <div class="container">
		    <?php the_content() ?>

		    <div class="firula">
		      <img src="<?php bloginfo('template_url') ?>/img/firulas.png" alt="" />
		    </div>


		  </div>

		</div>

    <?php
		  endwhile;	?>

<?php get_footer(); ?>
