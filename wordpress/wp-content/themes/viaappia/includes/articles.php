<?php $args = array(
              'post_type'			=> 'post',
              'posts_per_page'	=> 8,
              'paged' => get_query_var('paged')
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>
  <ul>
    <?php while (have_posts()) : the_post(); ?>
      <li>
        <a href="<?= get_permalink(); ?>">
          <div class="text">
            <div class="category">
              <?php
  								$post_categories = wp_get_post_categories(get_the_ID());

  								foreach($post_categories as $c){
  									$cat = get_category( $c ); ?>


  						            <span><?php echo $cat->name; ?></span>

  							<?php

  								}

  							?>
            </div>
            <h2><?php the_title() ?></h2>
            <h4>by: <?php the_author(); ?></h4>
  		      <h4><?php the_date('Y/m/d'); ?></h4>

		      </div>


        </a>
      </li>
    <?php endwhile; ?>
  </ul>

</div>

<div class="container paginationContainer">
  <div class="pagination">
      <?php bm_numeric_pagination($query); ?>
  </div>


<?php endif; ?>

<?php wp_reset_query(); ?>
