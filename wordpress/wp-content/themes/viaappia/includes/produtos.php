<?php $args = array(
              'post_type'			=> 'produto',
              'posts_per_page'	=> -1
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>
  <ul class="produtos">
    <?php while (have_posts()) : the_post(); ?>
      <li>
        <a href="<?= get_permalink() ?>">
          <div class="icon">
            <?php if ( has_post_thumbnail() ) { ?>

              <img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="" />

            <?php } else { ?>

              <img src="<?php bloginfo('template_url'); ?>/img/portfolio/xml.png" alt="<?php the_title(); ?>" />

            <?php } ?>

          </div>
          <h2><?php the_title(); ?></h2>
          <p>
            <?php the_content(); ?>
          </p>
          <div class="seta">
            <i class="icon-arrow"></i>
          </div>
        </a>
      </li>
    <?php endwhile; ?>
  </ul>

<?php endif; ?>

<?php wp_reset_query(); ?>
