<?php $args = array(
              'post_type'			=> 'clientes',
              'posts_per_page'	=> 4
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>
  <ul class="clientes">
		<li>CLIENTES:</li>
    <?php while (have_posts()) : the_post(); ?>
			<li><a href="<?= get_field('link') ?>" target="_blank"><img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>" alt="" /></a></li>
    <?php endwhile; ?>
  </ul>

<?php endif; ?>

<?php wp_reset_query(); ?>
