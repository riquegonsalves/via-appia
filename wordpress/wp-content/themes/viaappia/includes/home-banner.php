<section class="banner">
  <div class="container">
    <?php if(get_field('segmentos')): ?>

        <?php $contador = 1; while(has_sub_field('segmentos')): ?>
          <a href="<?= get_post_permalink(get_sub_field('segmento')->ID) ?>" class="slice">
            <div class="">
              <h1 style="text-transform: uppercase;"><?= get_sub_field('segmento')->post_title ?></h1>
              <h3><?= get_sub_field('segmento')->post_content ?></h3>
            </div>
          </a>

        <?php $contador++; endwhile; ?>

    <?php endif; ?>
  </div>
  <div class="seta">
    <div class="circle"><span> </span></div>
  </div>
</section>
